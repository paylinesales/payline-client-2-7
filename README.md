# Payline Client

## Technologies Used

- NextJS
- Apollo
- GraphQL
- TypeScript
- TailwindCSS
- ESLint
- Prettier
- Husky
- Graphql-codegen

## Prerequisites

- NodeJS
- NPM
- Yarn

## Getting started
The setup has been done using [NextJS](https://nextjs.org/).

Clone the repository.


## Installation

Install the dependencies using [Yarn](https://yarnpkg.com/en/docs/install).
``` 
yarn
```

## Running the application

Run the development server using [NextJS](https://nextjs.org/docs/basic-features/nextjs-with-typescript).
```
yarn dev
```

## Making Commits

This project uses husky to manage pre-commit checks. 
Make sure you address all the errors that arrise from the pre-commit checks before committing.

