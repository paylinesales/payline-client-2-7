const PRODUCT_TYPES = {
  SOFTWARE: "software",
  HARDWARE: "hardware",
};

const BUSINESS_TYPES = {
  SMALL_BUSINESS: "small-business",
  ENTERPRISE: "enterprise",
};

const PAYMENT_TYPES = {
  ONLINE: "online",
  IN_PERSON: "in-person",
};

enum ICON_POSITION {
  LEFT = 0,
  RIGHT = 1,
}

export { PRODUCT_TYPES, BUSINESS_TYPES, PAYMENT_TYPES, ICON_POSITION };
