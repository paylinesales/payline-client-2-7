/* eslint-disable  react/require-default-props, react/no-unused-prop-types, react/forbid-prop-types */

import React, { ReactElement } from "react";
import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import WhiteButton from "@/components/UI/atoms/WhiteButton";
import searchFilterQuery from "../../../queries/search-filter";

interface ICategoriesPickerProps {
  categories?: Array<string>;
  setCategories?: React.Dispatch<React.SetStateAction<string[]>>;
}

const CategoriesPicker: React.FC<ICategoriesPickerProps> = (
  props,
): ReactElement => {
  const { categories = [""], setCategories } = props;
  const { loading, error, data } = useQuery(searchFilterQuery);
  if (loading) {
    return <></>;
  }
  if (error) {
    return (
      <h1 className="px-4 text-4xl text-payline-black">
        Error loading categories: {error}
      </h1>
    );
  }

  const categoriesHolder: Array<{ name: string; isActive: boolean }> = [];
  data.categories.nodes.forEach((category) => {
    categoriesHolder.push({
      name: category.name,
      isActive: categories.indexOf(category.name) > -1,
    });
  });

  if (setCategories) {
    return (
      <div className="flex flex-wrap gap-3">
        {categoriesHolder.map((c) => {
          return (
            <WhiteButton
              key={c.name}
              className="rounded-full"
              selected={c.isActive}
              onClick={() => {
                // eslint-disable-next-line no-param-reassign
                c.isActive = !c.isActive;
                if (categories.indexOf("") > -1) {
                  setCategories([c.name]);
                } else if (categories.indexOf(c.name) > -1) {
                  setCategories(
                    categories.filter((category) => category !== c.name),
                  );
                } else {
                  setCategories((oldCategories) => [...oldCategories, c.name]);
                }
              }}>
              <b>{c.name}</b>
            </WhiteButton>
          );
        })}
      </div>
    );
  }
  const router = useRouter();
  return (
    <div className="flex flex-wrap gap-3">
      {categoriesHolder.map((c) => {
        return (
          <WhiteButton
            key={c.name}
            className="rounded-full"
            selected={c.isActive}
            onClick={() => {
              const categoryName = c.name;
              router.push({
                pathname: "/blog-search",
                query: `category=${categoryName}`,
              });
            }}>
            <b>{c.name}</b>
          </WhiteButton>
        );
      })}
    </div>
  );
};

export default CategoriesPicker;
