import React from "react";
import SectionTitle from "@/components/UI/atoms/SectionTitle";
import PaylineDetail from "@/components/UI/atoms/PaylineDetail";
import QuestionsAboutSolutions from "./QuestionsAboutSolutions";

const details = [
  {
    title: "First Two Months Free",
  },
  {
    title: "Dedicated Account Manager and Support Team",
  },
  {
    title: "Statement Analysis",
  },
  {
    title: "Statement Analysis, Onboarding session, First Two Months Free ",
  },
];

const LetsGetInTouchSection: React.FC = () => {
  return (
    <section
      id="lets-get-in-touch-section"
      className="container mx-auto  my-0 lg:my-12 py-10 px-8 md:px-16 lg:px-20 xl:px-9">
      <SectionTitle firstPartOfHeadline="Let's get" highlighedText="in touch" />
      <div className="flex flex-wrap my-10 lg:my-24">
        <QuestionsAboutSolutions />
        <div className="w-full lg:w-1/2 order-1 lg:order-none lg:px-12 hidden lg:block">
          {details.map((detail) => (
            <PaylineDetail
              key={detail.title}
              title={detail.title}
              className="border border-solid border-payline-border-light rounded bg-transparent hw-details transition-colors duration-150"
              summaryClasses="text-lg text-payline-black">
              <p>
                We put our money where our mouth is, check us out with no risk
              </p>
            </PaylineDetail>
          ))}
        </div>
      </div>
    </section>
  );
};

export default LetsGetInTouchSection;
