import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import {
  WebsiteCopy_Calculatorsection_InPersonData as InPersonDataTypes,
  WebsiteCopy_Calculatorsection_OnlineData as OnlineDataTypes,
} from "@/generated/apolloComponents";
import PlansIcon from "./PlansIcon";
import { PAYMENT_TYPES } from "../../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;

interface IProps {
  inPersonData: InPersonDataTypes[];
  onlineData: OnlineDataTypes[];
}
const Plans = (props: IProps): React.ReactElement => {
  const { inPersonData, onlineData } = props;
  const { paymentType } = useBusinessCategory();

  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;

  const currentPlan = isOnlineBusiness ? onlineData : inPersonData;
  return (
    <div className="my-5 flex flex-wrap gap-6 justify-start lg:justify-center bg-payline-white lg:bg-transparent py-9 lg:py-0 border-t-2 lg:border-t-0 border-b-2 lg:border-b-0 border-payline-border-light -mx-2 lg:-mx-0 px-9 lg:px-0">
      {currentPlan.map((p) => {
        return (
          <div
            className="flex-grow filter drop-shadow-none lg:drop-shadow-md bg-payline-white border-none lg:border-solid lg:border-payline-border-light rounded border border-solid border-payline-border-light rounded text-payline-dark p-3"
            key={p.label}>
            <div className="flex justify-none lg:justify-center items-center gap-4">
              <PlansIcon />
              <div>{p.label}</div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Plans;
