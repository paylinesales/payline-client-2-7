import React from "react";
import Image from "next/dist/client/image";
import { PAYMENT_TYPES } from "../../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;

type PricingCardProps = {
  className: string;
  ImageSrc: string;
  cardTitle: string;
  cardPercentage: number;
  businessType: string;
};

const PricingCard: React.FC<PricingCardProps> = ({
  className: classes,
  ImageSrc,
  cardTitle,
  cardPercentage,
  businessType,
}) => {
  const cardColor = businessType === ONLINE ? "payline-green" : "payline-blue";
  return (
    <div
      className={`bg-white rounded-lg filter relative drop-shadow-md flex flex-col justify-center mt-24 pb-16 px-6 ${classes}`}>
      <div className="-mt-20 flex justify-center">
        <Image src={ImageSrc} width={150} height={150} />
      </div>
      <p className="text-payline-black text-2xl font-bold flex justify-center mt-8 mb-2">
        {cardTitle}
      </p>
      <p className={`font-bold text-${cardColor} text-7xl mb-6 text-center`}>
        {cardPercentage}%
      </p>
      <p className="text-center text-payline-dark">
        $0.20 per transaction / $20 per month.
      </p>
    </div>
  );
};

export default PricingCard;
