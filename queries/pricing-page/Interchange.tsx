import { gql } from "@apollo/client";

const interchangeQuery = gql`
  query interChangeQuery {
    pages(where: { title: "Interchange Rate & Pricing" }) {
      edges {
        node {
          title
          slug
          id
          interchangeRate {
            cardAccordion {
              accordion {
                cardLabelRepeater {
                  cardLabel
                  cardTypeRepeater {
                    cardType
                    fieldGroupName
                    cardTypeDetails {
                      fieldGroupName
                      itemDetailsText
                      priceDetails
                    }
                  }
                  fieldGroupName
                }
              }
            }
            creditCardLogos {
              americanExpress {
                altText
                sourceUrl
                title
                id
              }
              discover {
                altText
                sourceUrl
                title
                id
              }
              mastercard {
                altText
                id
                sourceUrl
                title
              }
              visa {
                altText
                id
                title
                sourceUrl
              }
            }
            interchangeImage1 {
              altText
              sourceUrl
            }
            interchangeImage2 {
              altText
              sourceUrl
            }
          }
        }
      }
    }
  }
`;

export default interchangeQuery;
