import { gql } from "@apollo/client";

export const homeBlogQueries = gql`
  query homeBlogQueries {
    categories(first: 10) {
      edges {
        node {
          name
          slug
        }
      }
    }
    posts(first: 10) {
      edges {
        node {
          title
          slug
          date
          featuredImage {
            node {
              mediaItemUrl
            }
          }
        }
      }
    }
  }
`;

export const blogPageQuery = gql`
  query blogPageQueries($first: Int, $after: String) {
    posts(first: $first, after: $after) {
      edges {
        node {
          title
          slug
          date
          featuredImage {
            node {
              sourceUrl
            }
          }
          author {
            node {
              url
              firstName
              lastName
              avatar {
                url
              }
              roles {
                edges {
                  node {
                    displayName
                  }
                }
              }
            }
          }
          id
          categories {
            edges {
              node {
                name
              }
            }
          }
          excerpt(format: RENDERED)
        }
        cursor
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`;

export const blogArticleQuery = gql`
  query blogArticleQuery($id: ID!) {
    post(id: $id, idType: SLUG) {
      featuredImage {
        node {
          sourceUrl
        }
      }
      content(format: RENDERED)
      author {
        node {
          avatar {
            url
          }
          name
        }
      }
      date
      slug
      title
      categories {
        edges {
          node {
            name
          }
        }
      }
    }
  }
`;

export const blogSlugQuery = gql`
  query blogSlugQuery {
    posts {
      edges {
        node {
          slug
        }
      }
    }
  }
`;
