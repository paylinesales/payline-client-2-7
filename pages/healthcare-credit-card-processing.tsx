import Head from "next/head";
import React from "react";
import { BsPhone } from "react-icons/bs";
import { FiRefreshCcw } from "react-icons/fi";
import { MdOutlineAttachMoney } from "react-icons/md";
import { AiOutlineUnlock } from "react-icons/ai";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";

const pageData = {
  hero: {
    text: "Payment Solutions for your",
    title: "Medical Office",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const Medical = () => {
  return (
    <>
      <Head>
        <title>Healthcare Payment Processing Solutions | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                {pageData.hero.text}
                <br />
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="w-full md:w-6/12 px-4 my-20 text-center mx-auto">
        <div className="flex flex-wrap">
          <div className="w-full mb-10">
            <h3 className="text-lg text-payline-black font-bold text-3xl ">
              All the Features Your Office Needs for a Happy Staff
            </h3>
          </div>
          <div className="w-full md:w-6/12 px-4">
            <div className="relative flex flex-col mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <BsPhone className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  ACCEPT CHIPS & MOBILE PAYMENTS
                </h6>
                <p className="mb-4 text-color">
                  Your business won’t skip beat with EMV/Chip & mobile payment
                  services. Give your patients what they want.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <MdOutlineAttachMoney className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  SIMPLE TRANSPARENT PRICING
                </h6>
                <p className="mb-4 text-color">
                  This extension also comes with 3 sample pages. They are fully
                  coded so you can start working instantly.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-6/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <FiRefreshCcw className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  SEAMLESS INTEGRATION
                </h6>
                <p className="mb-4 text-color">
                  We also feature many dynamic components for React, NextJS, Vue
                  and Angular.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineUnlock className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  HIPAA COMPLIANT
                </h6>
                <p className="mb-4 text-color">
                  Add an additional layer of security by encrypting your
                  customer payment information with our secure HIPAA Compliant
                  devices, software, and virtual terminal.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              PRIVATE PAYMENT SOLUTIONS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Mobile Readers & Apps
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Use a mobile device to take payments away from the front office.
              Multiple solutions ready to go out-of-the box by downloading a
              mobile app or flexible WiFi solutions. All-in-one mobile devices
              that take payments and print receipts. Swipe, Chip, Tap.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/iStock-1134679866-scaled-1.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              SECURE COUNTERTOP CARD READERS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Ingenico Devices
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Get a secure tabletop payment solution from Ingenico, the world’s
              leader in devices. Accept all electronic payment methods including
              EMV/chip & PIN, magstripe and NFC/contactless. These devices runs
              on the most up to date Telium 2 operating system – a reliable,
              economical, and secure option for accepting and processing
              payments. Swipe, Chip, Tap.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              MULTIPLE WAYS TO ACCEPT PAYMENTS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Card Not Present Solutions
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Billing can get complicated and we provide a suite of tools to
              track down payments while lightening the load on your staff.
              Payline offers robust invoicing software, recurring & scheduled
              billing, a secure payments page to add to your website, electronic
              check (ACH) payments, and a virtual terminal to key credit cards
              in. Unlimited users and dozens of offices all for the same price.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <section className="bg-payline-blue mt-20">
        <div className="container mx-auto p-20">
          <div className="w-9/12 mx-auto text-center">
            <h3 className="text-lg text-white font-bold text-4xl mb-5">
              Ask about our special medical rates
            </h3>
            <a
              href="#"
              className="rounded-full bg-payline-black text-white px-5 py-2 text-center">
              CONTACT SALES
            </a>
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default Medical;
