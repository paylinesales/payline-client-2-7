import Head from "next/head";
import React from "react";
import { AiOutlineCheck } from "react-icons/ai";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";

const pageData = {
  hero: {
    text: "Credit Card",
    title: "Fraud Protection",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const CreditCardFraud = () => {
  return (
    <>
      <Head>
        <title>Credit Card Fraud Protection | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                {pageData.hero.text}
                <br />
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="w-full px-4 my-20 text-center mx-auto">
        <div className="flex flex-wrap justify-center align-center">
          <div className="w-full mb-10">
            <h3 className="text-lg text-payline-black font-bold text-3xl ">
              Stand Up to Fraudsters
            </h3>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  CONFIRM YOUR CUSTOMERS
                </h6>
                <p className="mb-4 text-color">
                  Include a CVV verification at checkout to authenticate the
                  verification number on the back of a customer’s card.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  ADDRESS VERIFICATION
                </h6>
                <p className="mb-4 text-color">
                  Trust that your customers are true cardholders with our AVS
                  check solution which validates address information.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  PROTECT PAYMENTS
                </h6>
                <p className="mb-4 text-color">
                  Configure filters to help determine fraud and differentiate
                  between legitimate and suspicious transactions with our
                  rules-based fraud solution.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Confirm Your Customers
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              For card-not-present transactions, requesting a card verification
              value, or CVV, at checkout is one method to guarantee credit card
              fraud protection and management. This three- or four- digit code
              serves as proof that the customer actually holds the physical
              card, keeping both the merchant and the customer safe by reducing
              the chance for fraud.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image5.jpg"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Address Verification
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              When used with other credit card fraud protection mechanisms, an
              address verification system (AVS) check validates address
              information of card-not-present customers. This secure process
              compares the billing address used in the transaction with the
              issuing bank’s address information on file for the cardholder. A
              full information validation is just one way that credit card fraud
              protection can be provided in card-not-present transactions.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Protect Payments
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Fraudsters continue their attempts to outsmart business owners,
              costing businesses hundreds, maybe thousands of dollars. To
              prevent this ongoing threat, you need a rules-based credit card
              fraud protection tool that can screen suspicious transaction
              activity. Stay one step ahead of fraud with solutions from
              Payline.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <section className="bg-payline-blue mt-20">
        <div className="container mx-auto p-20">
          <div className="w-9/12 mx-auto text-center">
            <h3 className="text-lg text-white font-bold text-4xl mb-5">
              Secure Payments With Credit Card Fraud Protection
            </h3>
            <p className="text-white">
              Your merchant account matters as much to us as it does to you.
              Programs like Verifi and Ethoca not only limit chargebacks on your
              account, but also stop fraud from happening.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Verifi
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Payline is an Authorized Reseller of Verifi’s award-winning
              Cardholder Dispute Resolution Network® (CDRN), which not only
              helps to protect your business by stopping chargebacks but also
              helps to boost transaction volume and your revenue.
            </p>
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default CreditCardFraud;
