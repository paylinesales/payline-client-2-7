import Head from "next/head";
import React from "react";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";

const pageData = {
  hero: {
    text: "Simple, Secure Online",
    title: "Gateway Payment Processing",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const Gateway2 = () => {
  return (
    <>
      <Head>
        <title>Payment Gateway - Secure Online Processing | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                {pageData.hero.text}
                <br />
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-2 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              eCOMMERCE
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Web and Mobile Gateway Payment Processing
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              With Payline’s gateway payment processing, you can seamlessly
              integrate with a growing list of 175+ Online Shopping Cart
              Integrations. It’s the perfect solution for accepting credit,
              debit and electronic payments online. Or integrate your app using
              our REST gateway API.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image5.jpg"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h2 className="mb-2 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              FLEXIBLE INTEGRATION METHODS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Your Software Your Payment Process
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Our Gateway and Mobile APIs give you full customization and
              control over accepting payments within your mobile applications
              and software.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-2 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              Payline Connect
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Payment Processing With Built in Fraud-Protection
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Payline’s gateway provides built-in security and fraud protection
              that’s easy to integrate into any payment experience.
              <ul className="list-disc pl-10 mt-3">
                <li>Shopping cart integrations</li>
                <li>APIs & SDKs</li>
                <li>Gateway-only solutions</li>
                <li>Payments page</li>
                <li>Authorize.net middleware plugin</li>
                <li>Quickbooks integrations</li>
              </ul>
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default Gateway2;
