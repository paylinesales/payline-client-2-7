import Head from "next/head";
import React from "react";
import { AiOutlineCheck } from "react-icons/ai";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";

const pageData = {
  hero: {
    text: "Payline Guarantees Data Security",
    title: "PCI Compliance",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const DataSecurity = () => {
  return (
    <>
      <Head>
        <title>PCI Compliance Data Security | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                {pageData.hero.text}
                <br />
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="w-full px-4 my-20 text-center mx-auto">
        <div className="flex flex-wrap justify-center align-center">
          <div className="w-full mb-10">
            <h3 className="text-lg text-payline-black font-bold text-3xl ">
              Stand Up to Fraudsters
            </h3>
            <p className="text-color">
              Earn your customers’ trust by protecting their sensitive
              information.
            </p>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  ACCOUNT UPDATER
                </h6>
                <p className="mb-4 text-color">
                  Maintain the accuracy of your customer data and prevent
                  transaction disruption caused by account changes.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  LEVEL 3 DATA
                </h6>
                <p className="mb-4 text-color">
                  Pass on enhanced transaction data and qualify for reduced
                  interchange costs.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  CUSTOMER VAULT
                </h6>
                <p className="mb-4 text-color">
                  Allows merchants to initiate transaction remotely without
                  having to directly access cardholder information.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              PCI Compliance
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Following PCI compliance requirements isn’t optional for
              businesses who accept cedit and debit card payments, in large part
              because of the growing threat of security breaches. Invest in the
              right security measures to protect yourself and your loyal
              customers.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image5.jpg"
            />
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default DataSecurity;
