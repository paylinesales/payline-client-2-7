import Head from "next/head";
import React from "react";
import { AiOutlineCheck } from "react-icons/ai";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";

const pageData = {
  hero: {
    text: "Payment Processing",
    title: "ACH",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const Ach = () => {
  return (
    <>
      <Head>
        <title>ACH Payment Processing | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
                <br />
                {pageData.hero.text}
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="w-full px-4 my-20 text-center mx-auto">
        <div className="flex flex-wrap justify-center align-center">
          <div className="w-full mb-10">
            <h3 className="text-lg text-payline-black font-bold text-3xl ">
              Open Up a Window of Opportunity
            </h3>
            <p className="text-color">
              There are tremendous benefits to making ACH solutions available to
              your customers or by using the ACH network for administrative
              functions.
            </p>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  BETTER PAYMENT REASSURANCE
                </h6>
                <p className="mb-4 text-color">
                  Give your business the advantage of knowing when you will be
                  paid, and be alerted faster if there are any issues.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  BETTER RECURRING PAYMENTS
                </h6>
                <p className="mb-4 text-color">
                  No more manual processing of those recurring payments, which
                  leaves less room for payment processing errors.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  LESS PAPER, MORE CONVENIENCE
                </h6>
                <p className="mb-4 text-color">
                  The ability to process settlements multiple times a day
                  provides a more convenient way for you to stay on top of
                  balancing your books.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  SAFE & SECURE
                </h6>
                <p className="mb-4 text-color">
                  Thanks to better encryption technology, an electronic check
                  payment can better protect your payments than when dealing
                  with old-school paper checks.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-3/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  DITCH THE FEES
                </h6>
                <p className="mb-4 text-color">
                  Payments are processed through an electronic network in
                  batches and will be be lower than credit card fees.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineCheck className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  CONVERT CUSTOMERS FASTER
                </h6>
                <p className="mb-4 text-color">
                  Consumers like to have choices and additional payment options
                  means increased likelihood to convert at checkout.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Better Payment Reassurance
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Your business deserves to know when you will be paid, and with ACH
              solutions from Payline, you will be alerted faster if there are
              any issues with receiving funds into your business account. With
              an ACH payment processor, your business is able to:
              <ul className="list-disc pl-10 mt-3">
                <li>
                  Get paid faster by cutting out the need for paper checks
                </li>
                <li>
                  Save time best spent elsewhere by avoiding trips to the bank
                </li>
              </ul>
              By accepting credit cards through merchant bank relationships or
              setting up recurring payments that come directly from your
              customers’ bank accounts, your business can get paid faster — and
              without interruption.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image5.jpg"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Less Paper, More Convenience
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Balance your books efficiently with the ability to process ACH
              settlements multiple times a day. An ACH payment solution can:
              <ul className="list-disc pl-10 mt-3">
                <li>Do away with checks and frequent trips to the bank</li>
                <li>
                  Save your customers time paying bills by creating a seamless,
                  carefree process
                </li>
                <li>
                  Scale back the likelihood of identity theft by removing the
                  need for paper statements
                </li>
              </ul>
              Relying on ACH payment solutions can be beneficial: not only will
              your business be seen as environmentally friendly, your customers
              will appreciate the convenient payment opportunities provided by
              your processing.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Ditch the Fees
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              An ACH payment is processed through an electronic network in
              payment groups referred to as “batches” and the cost can sometimes
              be lower than standard credit card processing fees. An ACH payment
              network will also:
              <ul className="list-disc pl-10 mt-3">
                <li>
                  Transmit payments electronically, increasing cost savings and
                  decreasing processing times
                </li>
                <li>
                  Typically cost less than $0.10 per transaction thanks to batch
                  processing as opposed to processing a paper check which costs
                  over a dollar on average.
                </li>
              </ul>
              Lower ACH processing fees might be the most obvious benefit of ACH
              solutions, but cost reduction can also help your business reach
              new heights faster than planned.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Better Recurring Payments
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Avoid human error by removing manual processing of recurring
              payments. Whether you’re accepting or making payments through the
              ACH payment network, it’s worth noting that:
              <ul className="list-disc pl-10 mt-3">
                <li>
                  Automatic invoicing and payments allow you or your customers
                  to run on autopilot, leaving one less thing to worry about
                  when it comes to getting paid
                </li>
                <li>
                  Customers must authorize the billing schedule before you
                  institute recurring bill pay
                </li>
              </ul>
              If you routinely charge customers large amounts on a consistent
              basis, make it easier to run your business with opportunities
              provided by ACH payment processing. Balance your books efficiently
              with the ability to process ACH settlements multiple times a day.
              An ACH payment solution can:
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Safe & Secure With ACH Payment
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              ACH payment processing can better protect your payments than when
              dealing with old-school paper checks, thanks to better encryption
              technology. ACH solutions provide:
              <ul className="list-disc pl-10 mt-3">
                <li>Secure encryption of all payments processed</li>
                <li>
                  A trusting relationship between you and your customers thanks
                  to the provided security of their information
                </li>
              </ul>
              Give peace of mind to you and your customers with ACH payment
              solutions by protecting customer credentials and safeguarding your
              business’ profits.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Clover_Flex_right-bg.png"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/Gateway_Page_Image13.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Convert Customers Faster
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Customers want choices, and offering multiple payment options
              means that customers are more likely to convert at checkout. With
              an ACH solution, you can expect:
              <ul className="list-disc pl-10 mt-3">
                <li>
                  A smooth checkout process from start to finish with the
                  customization options for online payments
                </li>
                <li>
                  Loyalty from customers who find your process move convenient
                  and secure than your competition
                </li>
              </ul>
              A seamless shopping experience will keep your customers coming
              back for more from your business. Offering ACH payment solutions
              is just one way in which you can build upon your reputation with
              your loyal fans.
            </p>
          </div>
        </div>
      </section>

      <section className="bg-payline-blue mt-20">
        <div className="container mx-auto p-20">
          <div className="w-9/12 mx-auto text-center">
            <h3 className="text-lg text-white font-bold text-4xl mb-5">
              The ACH Payment Network Can Help Your Business Scale
            </h3>
            <p className="text-white">
              When thinking about scaling your business and expanding your
              customer base, be careful not to overlook an ACH payment option
              that can reduce transaction costs to grow revenue. The benefits of
              processing transactions through ACH go beyond ordinary
              cost-savings. If you are a business owner looking to take your
              business to the next level, the solutions from Payline can be a
              lower-cost option compared to the standard way of taking payments,
              all thanks to the method of processing through an electronic
              network in large batches of both credit and debit card
              transactions. While digital payments are not a futuristic concept,
              their impact is shaping the future of payments – and the growth
              potential of your enterprise business. Expensive, burdensome
              processes like paper checks or manual payment processing hinder
              your productivity, and ACH solutions from Payline hold the key to
              helping your business reach that next stage of growth efficiently
              and without breaking the bank.
            </p>
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default Ach;
