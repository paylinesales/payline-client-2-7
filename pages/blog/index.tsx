/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ReactElement } from "react";
import client from "lib/apolloClient";
import BlogLayout from "@/components/UI/layouts/BlogLayout";
import BlogArticlesSection from "@/components/UI/templates/sections/blog/BlogArticlesSection";
import CategoriesSection from "@/components/UI/templates/sections/blog/CategoriesSection";
import RecentArticlesSection from "@/components/UI/templates/sections/blog/RecentArticlesSection";
import RecentArticlesSectionMobile from "@/components/UI/templates/sections/blog/RecentArticlesSectionMobile";
import { blogPageQuery } from "../../queries/blog";
import { allCategoryQuery } from "../../queries/categories";

interface BlogProps {
  blogsData: string;
  categoriesData: string;
}

export default function Blog({
  blogsData,
  categoriesData,
}: BlogProps): ReactElement {
  const parsedBlogData = JSON.parse(blogsData);
  const parsedCategoryData = JSON.parse(categoriesData);
  const { posts } = parsedBlogData.data;
  const { categories } = parsedCategoryData.data;
  return (
    <BlogLayout>
      <RecentArticlesSection postsData={posts} />
      <RecentArticlesSectionMobile />
      <CategoriesSection categoriesData={categories} />
      <BlogArticlesSection postsData={posts} />
    </BlogLayout>
  );
}

export async function getServerSideProps() {
  const blogs = await client.query({
    query: blogPageQuery,
    variables: {
      first: 500,
      after: null,
    },
  });

  const categories = await client.query({
    query: allCategoryQuery,
  });

  const blogsData = JSON.stringify(blogs);
  const categoriesData = JSON.stringify(categories);
  return {
    props: {
      blogsData,
      categoriesData,
    },
  };
}
